package com.progressoft.poc.mpgsdemo.utils;

import com.github.wnameless.json.flattener.JsonFlattener;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Base64;
import java.util.Map;

public class SessionDataDecrypter {

    private static final String SYMMETRIC_ALGORITHM = "AES/GCM/NoPadding";
    private static final String SYMMETRIC_KEY_TYPE = "AES";

    public static Map<String, Object> decrypt(String encodedKey, String ciphertext, String nonce, String tag)
            throws SessionDataDecrypterException {
        try {
            byte[] key = Base64.getDecoder().decode(encodedKey);
            Key keySpec = new SecretKeySpec(key, SYMMETRIC_KEY_TYPE);

            byte[] encryptedBytes = Base64.getDecoder().decode(ciphertext);
            byte[] tagBytes = Base64.getDecoder().decode(tag);
            byte[] input = new byte[encryptedBytes.length + tagBytes.length];
            System.arraycopy(encryptedBytes, 0, input, 0, encryptedBytes.length);
            System.arraycopy(tagBytes, 0, input, encryptedBytes.length, tagBytes.length);

            byte[] iv = Base64.getDecoder().decode(nonce);
            GCMParameterSpec parameterSpec = new GCMParameterSpec(tagBytes.length * Byte.SIZE, iv);
            Cipher decrypter = Cipher.getInstance(SYMMETRIC_ALGORITHM);
            decrypter.init(Cipher.DECRYPT_MODE, keySpec, parameterSpec);

            byte[] decryptedBytes = decrypter.doFinal(input);
            String serializedString = new String(decryptedBytes, StandardCharsets.UTF_8);
            return JsonFlattener.flattenAsMap(serializedString);
        } catch (Exception e) {
            throw new SessionDataDecrypterException(e.getMessage());
        }
    }

    public static class SessionDataDecrypterException extends Exception {
        public SessionDataDecrypterException(String message) {
            super(message);
        }
    }
}
