package com.progressoft.poc.mpgsdemo.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentRequest {

    private String channel;

    private String currency;

    private BigDecimal amount;

    private String cardNumber;

    private String cardExpiryMonth;

    private String cardExpiryYear;

    private String ccv;
}
