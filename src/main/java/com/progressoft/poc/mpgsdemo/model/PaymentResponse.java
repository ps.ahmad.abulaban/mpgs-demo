package com.progressoft.poc.mpgsdemo.model;

import lombok.Data;

@Data
public class PaymentResponse {

    private String id;

    private String paymentStatus;

    private String redirectHtml;
}
