package com.progressoft.poc.mpgsdemo.model;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaymentStatus {
    public static final String INITIATED = "Initiated";
    public static final String PENDING_3DS = "Pending 3DS";
    public static final String AUTHORIZED = "Authorized";
    public static final String CAPTURED = "Captured";
    public static final String FAILED = "Failed";
    public static final String CANCELLED = "Cancelled";
}
