package com.progressoft.poc.mpgsdemo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Session {
    private final String sessionId;
    private final String aesKey;
}
