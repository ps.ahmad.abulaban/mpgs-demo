package com.progressoft.poc.mpgsdemo.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@Entity(name = "payments")
@Table(name = "payments")
public class PaymentEntity {

    @Id
    private String id;

    @Column(nullable = false)
    private Instant receivingDateTime;

    @Column(nullable = false)
    private String channel;

    @Column(nullable = false)
    private String currency;

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private String cardNumber;

    @Column(nullable = false)
    private String cardExpiryMonth;

    @Column(nullable = false)
    private String cardExpiryYear;

    @Column(nullable = false)
    private String ccv;

    @Column
    private String sessionId;

    @Column
    private String aesKey;

    @Column(nullable = false)
    private String status;

    @Column
    private String reason;
}
