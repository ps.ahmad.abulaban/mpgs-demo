package com.progressoft.poc.mpgsdemo.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity(name = "payment_logs")
@Table(name = "payment_logs")
public class PaymentLogEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private String id;

    @Column(nullable = false)
    private Instant timestamp;

    @Column(nullable = false)
    private String paymentId;

    @Column(nullable = false)
    private String operation;

    @Column(nullable = false, length = 4000)
    private String request;

    @Column(nullable = false, length = 4000)
    private String response;
}
