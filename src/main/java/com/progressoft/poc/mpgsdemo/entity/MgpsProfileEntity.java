package com.progressoft.poc.mpgsdemo.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity(name = "mpgs_profiles")
@Table(name = "mpgs_profiles")
public class MgpsProfileEntity {

    @Id
    private String channel;

    @Column(nullable = false)
    private String merchantId;

    @Column(nullable = false)
    private String apiUsername;

    @Column(nullable = false)
    private String apiPassword;

    @Column(nullable = false)
    private boolean support3ds;
}
