package com.progressoft.poc.mpgsdemo.service;

import com.progressoft.poc.mpgsdemo.entity.MgpsProfileEntity;
import com.progressoft.poc.mpgsdemo.entity.PaymentEntity;
import com.progressoft.poc.mpgsdemo.entity.PaymentLogEntity;
import com.progressoft.poc.mpgsdemo.model.PaymentRequest;
import com.progressoft.poc.mpgsdemo.model.PaymentResponse;
import com.progressoft.poc.mpgsdemo.model.PaymentStatus;
import com.progressoft.poc.mpgsdemo.model.Session;
import com.progressoft.poc.mpgsdemo.repository.MpgsProfileRepository;
import com.progressoft.poc.mpgsdemo.repository.PaymentLogRepository;
import com.progressoft.poc.mpgsdemo.repository.PaymentRepository;
import com.progressoft.poc.mpgsdemo.utils.SessionDataDecrypter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

@Component
@Slf4j
public class PaymentService {

    private static final String RESULT = "result";
    private static final String RESPONSE_GATEWAY_CODE = "response.gatewayCode";
    private static final String RESPONSE_GATEWAY_RECOMMENDATION = "response.gatewayRecommendation";
    private static final String SUCCESS = "SUCCESS";
    private static final String PENDING = "PENDING";
    private static final String APPROVED = "APPROVED";
    private static final String PROCEED = "PROCEED";

    @Autowired
    private MpgsOperationService mpgsOperationService;

    @Autowired
    private MpgsProfileRepository mpgsProfileRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentLogRepository paymentLogRepository;

    public PaymentResponse processPayment(PaymentRequest payment) {
        String id = UUID.randomUUID().toString();
        PaymentEntity paymentEntity = mapToPaymentEntity(payment, id);
        try {
            MgpsProfileEntity mpgsProfile = mpgsProfileRepository.findById(payment.getChannel())
                    .orElseThrow(() -> {
                        throw new PaymentProcessingException("Channel not supported");
                    });
            Session session = createSession(mpgsProfile, id);
            paymentEntity.setSessionId(session.getSessionId());
            paymentEntity.setAesKey(session.getAesKey());
            paymentRepository.save(paymentEntity);
            updateSession(mpgsProfile, paymentEntity);
            PaymentResponse paymentResponse = new PaymentResponse();
            paymentResponse.setId(id);
            if (mpgsProfile.isSupport3ds()) {
                initiateAuthentication(mpgsProfile, paymentEntity);
                String redirectHtml = authenticatePayer(mpgsProfile, paymentEntity);
                paymentEntity.setStatus(PaymentStatus.PENDING_3DS);
                paymentResponse.setPaymentStatus(paymentEntity.getStatus());
                paymentResponse.setRedirectHtml(redirectHtml);
            } else {
                authorize(mpgsProfile, paymentEntity);
                capture(mpgsProfile, paymentEntity);
                paymentEntity.setStatus(PaymentStatus.CAPTURED);
                paymentResponse.setPaymentStatus(paymentEntity.getStatus());
            }
            paymentRepository.save(paymentEntity);
            return paymentResponse;
        } catch (PaymentProcessingException e) {
            paymentEntity.setStatus(PaymentStatus.FAILED);
            paymentEntity.setReason(e.getMessage());
            paymentRepository.save(paymentEntity);
            throw e;
        }
    }


    public PaymentResponse processCallBackPayment(MultiValueMap<String, String> requestBody) {
        String id = requestBody.getFirst("order.id");
        if (Objects.isNull(id))
            throw new IllegalArgumentException("Order Id property not exist");
        PaymentEntity paymentEntity = paymentRepository.findById(id).orElseThrow(() -> {
            throw new IllegalArgumentException("Payment not exist with id " + id);
        });
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setId(id);
        try {
            if (!(SUCCESS.equals(requestBody.getFirst(RESULT)) && "PROCEED".equals(requestBody.getFirst("response.gatewayRecommendation"))))
                throw new PaymentProcessingException("3DS Authentication failed");
            validateEncryptedData(requestBody, paymentEntity.getAesKey(), id);
            MgpsProfileEntity mpgsProfile = mpgsProfileRepository.findById(paymentEntity.getChannel())
                    .orElseThrow(() -> {
                        throw new PaymentProcessingException("Channel not supported");
                    });
            authorize(mpgsProfile, paymentEntity);
            capture(mpgsProfile, paymentEntity);
            paymentEntity.setStatus(PaymentStatus.CAPTURED);
        } catch (PaymentProcessingException e) {
            paymentEntity.setStatus(PaymentStatus.FAILED);
            paymentEntity.setReason(e.getMessage());
        }
        paymentResponse.setPaymentStatus(paymentEntity.getStatus());
        paymentRepository.save(paymentEntity);
        return paymentResponse;
    }

    public void processStuckPayment(PaymentEntity payment) {
        String paymentId = payment.getId();
        log.info("Try to process stuck payment with id {}", paymentId);
        try {
            MgpsProfileEntity mpgsProfile = mpgsProfileRepository.findById(payment.getChannel())
                    .orElseThrow(() -> {
                        throw new PaymentProcessingException("Channel not supported");
                    });
            String orderStatus = retrieveOrder(mpgsProfile, paymentId);
            if (orderStatus.equals("CAPTURED"))
                payment.setStatus(PaymentStatus.CAPTURED);
            else if (orderStatus.equals("AUTHORIZED")) {
                voidPayment(mpgsProfile, paymentId);
                payment.setStatus(PaymentStatus.CANCELLED);
            } else
                payment.setStatus(PaymentStatus.CANCELLED);
            log.info("Stuck payment {} status changed to {}", paymentId, payment.getStatus());
            paymentRepository.save(payment);
        } catch (PaymentProcessingException e) {
            log.error("Process stuck payment with id {} failed", paymentId, e);
        }
    }

    private String retrieveOrder(MgpsProfileEntity mpgsProfile, String id) {
        String operation = "RETRIEVE_ORDER";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        requestBody.add("order.id", id);
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        validateResponse((e) -> SUCCESS.equals(e.getFirst(RESULT)), response, operation);
        String responseAsString = response.toString();
        responseAsString = responseAsString.substring(0, Math.min(responseAsString.length(), 4000));
        savePaymentLog(id, requestBody.toString(), responseAsString, operation);
        return response.getFirst("status");
    }

    private void voidPayment(MgpsProfileEntity mpgsProfile, String id) {
        String operation = "VOID";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        requestBody.add("order.id", id);
        requestBody.add("transaction.targetTransactionId", new StringBuilder().append(id).reverse().toString());
        requestBody.add("transaction.id", UUID.randomUUID().toString());
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        savePaymentLog(id, requestBody.toString(), response.toString(), operation);
        validateResponse((e) -> SUCCESS.equals(e.getFirst(RESULT)), response, operation);
    }

    private void validateEncryptedData(MultiValueMap<String, String> requestBody, String aesKey, String paymentId) {
        try {
            Map<String, Object> decryptedValue = SessionDataDecrypter.decrypt(aesKey, requestBody.getFirst("encryptedData.ciphertext"), requestBody.getFirst("encryptedData.nonce"), requestBody.getFirst("encryptedData.tag"));
            if (!(paymentId.equals(decryptedValue.get("order.id"))
                    && paymentId.equals(decryptedValue.get("transaction.id"))
                    && "AUTHENTICATION_SUCCESSFUL".equals(decryptedValue.get("transaction.authenticationStatus"))))
                throw new PaymentProcessingException("3DS Authentication failed");
        } catch (SessionDataDecrypter.SessionDataDecrypterException e) {
            log.error("validate encrypted data failed", e);
            throw new PaymentProcessingException("3DS Authentication failed");
        }
    }

    private void capture(MgpsProfileEntity mpgsProfile, PaymentEntity paymentEntity) {
        String operation = "CAPTURE";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        requestBody.add("session.id", paymentEntity.getSessionId());
        requestBody.add("order.id", paymentEntity.getId());
        requestBody.add("transaction.id", UUID.randomUUID().toString());
        requestBody.add("transaction.amount", paymentEntity.getAmount().toString());
        requestBody.add("transaction.currency", paymentEntity.getCurrency());
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        savePaymentLog(paymentEntity.getId(), requestBody.toString(), response.toString(), operation);
        validateResponse((e) -> SUCCESS.equals(e.getFirst(RESULT)) && APPROVED.equals(response.getFirst(RESPONSE_GATEWAY_CODE))
                , response, operation);
    }

    private void authorize(MgpsProfileEntity mpgsProfile, PaymentEntity paymentEntity) {
        String operation = "AUTHORIZE";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        requestBody.add("session.id", paymentEntity.getSessionId());
        String paymentId = paymentEntity.getId();
        requestBody.add("order.id", paymentId);
        requestBody.add("transaction.id", new StringBuilder().append(paymentId).reverse().toString());
        requestBody.add("authentication.transactionId", paymentId);
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        savePaymentLog(paymentId, requestBody.toString(), response.toString(), operation);
        validateResponse((e) -> SUCCESS.equals(e.getFirst(RESULT)) && APPROVED.equals(response.getFirst(RESPONSE_GATEWAY_CODE))
                , response, operation);
    }

    private String authenticatePayer(MgpsProfileEntity mpgsProfile, PaymentEntity paymentEntity) {
        String operation = "AUTHENTICATE_PAYER";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        requestBody.add("session.id", paymentEntity.getSessionId());
        requestBody.add("order.id", paymentEntity.getId());
        requestBody.add("transaction.id", paymentEntity.getId());
        requestBody.add("device.browser", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36");
        requestBody.add("device.browserDetails.3DSecureChallengeWindowSize", "FULL_SCREEN");
        requestBody.add("device.browserDetails.javaEnabled", "true");
        requestBody.add("device.browserDetails.javaScriptEnabled", "true");
        requestBody.add("device.browserDetails.colorDepth", "24");
        requestBody.add("device.browserDetails.screenHeight", "640");
        requestBody.add("device.browserDetails.screenWidth", "480");
        requestBody.add("device.browserDetails.timeZone", "273");
        requestBody.add("device.browserDetails.language", "en");
        requestBody.add("authentication.redirectResponseUrl", "http://localhost:8080/payment/3dsCallBack");
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        savePaymentLog(paymentEntity.getId(), requestBody.toString(), response.toString(), operation);
        validateResponse((e) -> PENDING.equals(e.getFirst(RESULT)) && PROCEED.equals(e.getFirst(RESPONSE_GATEWAY_RECOMMENDATION))
                , response, operation);
        return response.getFirst("authentication.redirectHtml");
    }

    private void initiateAuthentication(MgpsProfileEntity mpgsProfile, PaymentEntity paymentEntity) {
        String operation = "INITIATE_AUTHENTICATION";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        requestBody.add("session.id", paymentEntity.getSessionId());
        requestBody.add("order.id", paymentEntity.getId());
        requestBody.add("transaction.id", paymentEntity.getId());
        requestBody.add("authentication.channel", "PAYER_BROWSER");
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        savePaymentLog(paymentEntity.getId(), requestBody.toString(), response.toString(), operation);
        validateResponse((e) -> SUCCESS.equals(e.getFirst(RESULT)) && PROCEED.equals(e.getFirst(RESPONSE_GATEWAY_RECOMMENDATION))
                        && "AUTHENTICATION_AVAILABLE".equals(response.getFirst("transaction.authenticationStatus"))
                , response, operation);
    }

    private void updateSession(MgpsProfileEntity mpgsProfile, PaymentEntity paymentEntity) {
        String operation = "UPDATE_SESSION";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        requestBody.add("session.id", paymentEntity.getSessionId());
        requestBody.add("order.id", paymentEntity.getId());
        requestBody.add("order.amount", paymentEntity.getAmount().toString());
        requestBody.add("order.currency", paymentEntity.getCurrency());
        requestBody.add("sourceOfFunds.type", "CARD");
        requestBody.add("sourceOfFunds.provided.card.number", paymentEntity.getCardNumber());
        requestBody.add("sourceOfFunds.provided.card.securityCode", paymentEntity.getCcv());
        requestBody.add("sourceOfFunds.provided.card.expiry.month", paymentEntity.getCardExpiryMonth());
        requestBody.add("sourceOfFunds.provided.card.expiry.year", paymentEntity.getCardExpiryYear());
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        savePaymentLog(paymentEntity.getId(), requestBody.toString(), response.toString(), operation);
        validateResponse((e) -> SUCCESS.equals(e.getFirst("session.updateStatus")), response, operation);
    }

    private Session createSession(MgpsProfileEntity mpgsProfile, String id) {
        String operation = "CREATE_SESSION";
        MultiValueMap<String, String> requestBody = createRequestBody(mpgsProfile, operation);
        MultiValueMap<String, String> response = mpgsOperationService.post(requestBody);
        savePaymentLog(id, requestBody.toString(), response.toString(), operation);
        validateResponse((e) -> SUCCESS.equals(e.getFirst(RESULT)), response, operation);
        return new Session(response.getFirst("session.id"), response.getFirst("session.aes256Key"));
    }

    private void validateResponse(Function<MultiValueMap<String, String>, Boolean> validCondition, MultiValueMap<String, String> response, String operation) {
        if (!validCondition.apply(response)) {
            if ("ERROR".equals(response.getFirst(RESULT)))
                throw new PaymentProcessingException(response.getFirst("error.cause") + " - " + response.getFirst("error.explanation"));
            throw new PaymentProcessingException(operation + " operation failed");
        }
    }

    private void savePaymentLog(String id, String request, String response, String operation) {
        log.info("Operation {} done, response {}", operation, response);
        PaymentLogEntity paymentLog = new PaymentLogEntity();
        paymentLog.setTimestamp(Instant.now());
        paymentLog.setOperation(operation);
        paymentLog.setPaymentId(id);
        paymentLog.setRequest(request);
        paymentLog.setResponse(response);
        paymentLogRepository.save(paymentLog);
    }

    private MultiValueMap<String, String> createRequestBody(MgpsProfileEntity mpgsProfile, String operation) {
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("merchant", mpgsProfile.getMerchantId());
        requestBody.add("apiUsername", mpgsProfile.getApiUsername());
        requestBody.add("apiPassword", mpgsProfile.getApiPassword());
        requestBody.add("apiOperation", operation);
        return requestBody;
    }

    private PaymentEntity mapToPaymentEntity(PaymentRequest payment, String id) {
        PaymentEntity paymentEntity = new PaymentEntity();
        paymentEntity.setId(id);
        paymentEntity.setReceivingDateTime(Instant.now());
        paymentEntity.setChannel(payment.getChannel());
        paymentEntity.setCurrency(payment.getCurrency());
        paymentEntity.setAmount(payment.getAmount());
        paymentEntity.setCardNumber(payment.getCardNumber());
        paymentEntity.setCardExpiryMonth(payment.getCardExpiryMonth());
        paymentEntity.setCardExpiryYear(payment.getCardExpiryYear());
        paymentEntity.setCcv(payment.getCcv());
        paymentEntity.setStatus(PaymentStatus.INITIATED);
        return paymentEntity;
    }
}
