package com.progressoft.poc.mpgsdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class MpgsOperationService {

    @Value("${mpgs.apiUrl}")
    private String apiUrl;

    private final RestTemplate restTemplate = new RestTemplate();

    public MultiValueMap<String, String> post(MultiValueMap<String, String> requestBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestBody, headers);

        try {
            ResponseEntity<MultiValueMap> response = restTemplate.postForEntity(apiUrl, request, MultiValueMap.class);
            return (MultiValueMap<String, String>) response.getBody();
        } catch (Exception e) { //4xx or 5xx org.springframework.web.client.HttpClientErrorException$MethodNotAllowed: 405 Method Not Allowed:
            log.error(e.getMessage());
            throw new PaymentProcessingException("Failure while invoking mpgs api , " + e.getMessage());
        }
    }
}
