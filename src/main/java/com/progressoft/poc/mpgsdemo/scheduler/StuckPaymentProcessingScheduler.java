package com.progressoft.poc.mpgsdemo.scheduler;

import com.progressoft.poc.mpgsdemo.entity.PaymentEntity;
import com.progressoft.poc.mpgsdemo.model.PaymentStatus;
import com.progressoft.poc.mpgsdemo.repository.PaymentRepository;
import com.progressoft.poc.mpgsdemo.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class StuckPaymentProcessingScheduler {

    private static final List<String> FINAL_STATUSES = Arrays.asList(PaymentStatus.CAPTURED, PaymentStatus.FAILED, PaymentStatus.CANCELLED);

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentService paymentService;

    @Scheduled(fixedDelayString = "${stuck.payment.processing.interval}")
    public void process() {
        List<PaymentEntity> stuckPayments = paymentRepository.findAllByStatusNotInAndReceivingDateTimeLessThan(FINAL_STATUSES, Instant.now().minusSeconds(3600));
        log.info("Try to process stuck payments, number of payments is {}", stuckPayments.size());
        stuckPayments.forEach(p -> paymentService.processStuckPayment(p));
    }
}
