package com.progressoft.poc.mpgsdemo;

import com.progressoft.poc.mpgsdemo.model.PaymentRequest;
import com.progressoft.poc.mpgsdemo.model.PaymentResponse;
import com.progressoft.poc.mpgsdemo.model.PaymentStatus;
import com.progressoft.poc.mpgsdemo.service.PaymentProcessingException;
import com.progressoft.poc.mpgsdemo.service.PaymentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
@RequestMapping("/payment")
@Slf4j
public class PaymentController {

    @Autowired
    private PaymentService service;

    @PostMapping
    public ResponseEntity<PaymentResponse> post(@RequestBody PaymentRequest request) {
        log.info("New payment received {}", request);
        PaymentResponse response = service.processPayment(request);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/3dsCallBack")
    public ResponseEntity postCallBack(@RequestBody MultiValueMap<String, String> request, @RequestHeader HttpHeaders headers) {
        log.info("3DS call back payment received, request body {}", request);
        log.info("3DS call back payment received, request headers {}", headers.toString());
        PaymentResponse response = service.processCallBackPayment(request);
        if (response.getPaymentStatus().equals(PaymentStatus.CAPTURED))
            return ResponseEntity.status(HttpStatus.FOUND)
                    .header(HttpHeaders.LOCATION, "https://www.stc.com.kw")
                    .build();
        else
            return ResponseEntity.status(HttpStatus.FOUND)
                    .header(HttpHeaders.LOCATION, "https://www.google.com")
                    .build();

    }

    @ExceptionHandler(PaymentProcessingException.class)
    public ResponseEntity paymentProcessingExceptionHandler(Exception e) {
        log.error(e.getMessage());
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
