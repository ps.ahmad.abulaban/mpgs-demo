package com.progressoft.poc.mpgsdemo.repository;

import com.progressoft.poc.mpgsdemo.entity.PaymentLogEntity;
import org.springframework.data.repository.CrudRepository;

public interface PaymentLogRepository extends CrudRepository<PaymentLogEntity, String> {
}
