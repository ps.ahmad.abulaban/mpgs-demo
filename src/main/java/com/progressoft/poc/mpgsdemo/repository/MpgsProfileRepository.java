package com.progressoft.poc.mpgsdemo.repository;

import com.progressoft.poc.mpgsdemo.entity.MgpsProfileEntity;
import org.springframework.data.repository.CrudRepository;

public interface MpgsProfileRepository extends CrudRepository<MgpsProfileEntity, String> {
}
