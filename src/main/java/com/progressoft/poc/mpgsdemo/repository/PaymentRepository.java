package com.progressoft.poc.mpgsdemo.repository;

import com.progressoft.poc.mpgsdemo.entity.PaymentEntity;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;

public interface PaymentRepository extends CrudRepository<PaymentEntity, String> {

    List<PaymentEntity> findAllByStatusNotInAndReceivingDateTimeLessThan(List<String> statuses, Instant receivingDateTime);
}
